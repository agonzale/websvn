FROM cern/cc7-base
MAINTAINER Alvaro Gonzalez <agonzale@cern.ch>

COPY httpd/ /etc/httpd/conf.d/
COPY websvn-src /usr/share/websvn/

RUN yum install -y httpd subversion rsync php php-geshi php-xml php-pear php-mbstring php-geshi enscript && \
    # Reinstalling glibc-common, so we can have all locales installed. Something necessary for some hooks
    yum -y reinstall glibc-common && \
    yum clean all && \
    mkdir /var/svn && \
    #mkdir /etc/httpd/conf.d/configurable && \
    mkdir -p /run/httpd/ && chown -R root:root /run/httpd && chmod a+rw /run/httpd && \
    sed -i.bak 's/Listen 80/# Listen 80\nListen 8000/' /etc/httpd/conf/httpd.conf && \
    sed -i 's#^\s*ErrorLog.*#ErrorLog /dev/stderr#' /etc/httpd/conf/httpd.conf && \
    sed -i 's#^\s*CustomLog.*#CustomLog /dev/stdout combined#' /etc/httpd/conf/httpd.conf && \
    cp /usr/share/websvn/include/distconfig.php /usr/share/websvn/include/config.php && \
    echo "\$config->addRepository('repository', 'file:///var/svn/');" >>/usr/share/websvn/include/config.php && \
    echo "\$config->useAccessFile('/var/svn/conf/authz', 'repository');" >>/usr/share/websvn/include/config.php && \
    rm -rf /usr/share/httpd/noindex/*

EXPOSE 8000

USER apache:root

ENTRYPOINT ["httpd", "-D", "FOREGROUND"]
